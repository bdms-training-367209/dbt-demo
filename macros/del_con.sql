{% macro del_con(is_incremental, model, date_clmn_nm) %}

    {% if is_incremental == True %}
        -- table is existing
        {% if var('RUN_MODE') == 'I': %}
            truncate table {{ model }}
        {% elif var('RUN_MODE') == 'N' or var('RUN_MODE') == 'R': %}
            delete from {{ model }} WHERE date({{ date_clmn_nm }}) between '{{ var('FROM_BIZ_DT') }}' and '{{ var('TO_BIZ_DT') }}'
        {% endif %}
    {% endif %}

{% endmacro %}
