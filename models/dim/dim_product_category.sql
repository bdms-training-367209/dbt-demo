-- SCD TYPE2
-- Insert Dummy

with dummy as (
    select 
        '0000000000000000' as product_category_id, 
        'N/A' product_category_name,
        'N/A' product_sub_category_name,
        cast('1900-01-01' as timestamp) as active_from,
        cast(null as timestamp) as active_to,
        1 as active_flag,
        0 as source_product_category_id,
        0 as source_product_sub_category_id
    ),
    dim_product_category as (
        SELECT
            dbt_scd_id product_category_id,
            product_category_name as product_category_name,
            product_sub_category_name as product_sub_category_name,
            
            dbt_valid_from as  active_from,
            dbt_valid_to as active_to,
            if(dbt_valid_to is null, 1, 0) active_flag,

            source_product_category_id as source_product_category_id,
            source_product_sub_category_id as source_product_sub_category_id
        FROM {{ ref('snapshots_demo_production_category_vw') }}
    ),
    merge_dim as (
        select * from dummy
        union all 
        select * from dim_product_category
    )


-- CTE
select * from merge_dim --where active_to is null
