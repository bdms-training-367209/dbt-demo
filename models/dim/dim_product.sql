-- SCD TYPE2
-- Insert Dummy

with dummy as (
    select 
        '0000000000000000' as product_id, 
        'N/A' product_name,
        'N/A' product_number,
        'N/A' color,
        0 as safety_stock_level,
        0 as reoder_point,
        cast('1900-01-01' as timestamp) as active_from,
        cast(null as timestamp) as active_to,
        1 as active_flag,
        0 as source_product_id
    ),
    dim_product as (
        SELECT
            dbt_scd_id product_id,
            Name as product_name,
            ProductNumber as product_number,
            Color as color,
            SafetyStockLevel as safety_stock_level,
            ReorderPoint as reoder_point,

            dbt_valid_from as  active_from,
            dbt_valid_to as active_to,
            if(dbt_valid_to is null, 1, 0) active_flag,

            ProductID as source_product_id
        FROM {{ ref('snapshots_demo_production_product_vw') }}
    ),
    merge_dim as (
        select * from dummy
        union all 
        select * from dim_product
    )

-- CTE
select * from merge_dim --where active_to is null
