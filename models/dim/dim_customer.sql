-- SCD TYPE2
-- Insert Dummy

with dummy as (
    select 
        '0000000000000000' as customer_id, 
        0 as store_id,
        0 as territory_id,
        '0000000000' as account_number,
        cast('1900-01-01' as timestamp) as active_from,
        cast(null as timestamp) as active_to,
        1 as active_flag,
        0 as source_customer_id
    ),
    dim_customer as (
        SELECT
            dbt_scd_id customer_id,
            StoreID as store_id,
            TerritoryID as territory_id,
            AccountNumber as account_number,

            dbt_valid_from active_from,
            dbt_valid_to  active_to,
            if(dbt_valid_to is null, 1, 0) active_flag,

            CustomerID as source_customer_id
        FROM {{ ref('snapshots_demo_sales_customer_vw') }}

        WHERE StoreID is not null
    ),
    merge_dim as (
        select * from dummy
        union all 
        select * from dim_customer
    )

-- CTE
select * from merge_dim --where active_to is null
