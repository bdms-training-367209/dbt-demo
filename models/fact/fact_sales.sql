{{ config(pre_hook="{{ del_con(is_incremental(),this,'order_date') }}") }}

with  fact_sales as (
    SELECT
        order_date.date_id as order_date_id,
        due_date.date_id as due_date_id,
        ship_date.date_id as ship_date_id,
        dim_product.product_id,
        dim_product_category.product_category_id,
        dim_customer.customer_id,
        order_head.OrderDate as order_date,
        order_head.DueDate as due_date,
        order_head.ShipDate as ship_date,
        order_head.Status as status,
        order_head.SalesOrderNumber as sales_order_number,
        order_head.PurchaseOrderNumber as purchase_order_number,
        order_detail.SalesOrderID as source_sales_order_id,
        order_detail.SalesOrderDetailID as source_sales_order_detail_id,
        order_detail.CarrierTrackingNumber as source_carrier_tracking_number,
        order_detail.OrderQty as order_qty,
        order_detail.UnitPrice as unit_price,
        order_detail.UnitPriceDiscount as unit_price_discount,
        order_detail.LineTotal as line_total
    FROM {{ source('demo_db', 'demo_sales_salesorderheader') }} as order_head
    INNER JOIN {{ source('demo_db', 'demo_sales_salesorderdetail') }} as order_detail ON order_head.SalesOrderID = order_detail.SalesOrderID
    INNER JOIN {{ source('demo_model', 'dim_date') }} as order_date ON order_head.OrderDate = order_date.fulldate_en
    INNER JOIN {{ source('demo_model', 'dim_date') }} as due_date ON order_head.DueDate = due_date.fulldate_en
    INNER JOIN {{ source('demo_model', 'dim_date') }} as ship_date ON order_head.ShipDate = ship_date.fulldate_en
    INNER JOIN {{ ref('dim_product') }} as dim_product ON order_detail.ProductID = dim_product.source_product_id 
    AND date(order_head.orderdate) BETWEEN date(dim_product.active_from) AND IFNULL (date(dim_product.active_to),'9999-12-31')
    INNER JOIN {{ source('demo_db', 'demo_production_product') }} as demogls_product ON order_detail.ProductID = demogls_product.ProductID
    INNER JOIN {{ source('demo_db', 'demo_production_productsubcategory') }} as demogls_product_subcategory 
    ON demogls_product.ProductSubcategoryID = demogls_product_subcategory.ProductSubcategoryID
    INNER JOIN {{ ref('dim_product_category') }} as dim_product_category ON demogls_product_subcategory.ProductSubcategoryID = dim_product_category.source_product_sub_category_id 
    AND demogls_product_subcategory.ProductCategoryID = dim_product_category.source_product_category_id 
    AND date(order_head.orderdate) BETWEEN date(dim_product_category.active_from) AND IFNULL (date(dim_product_category.active_to),'9999-12-31')
    INNER JOIN {{ ref('dim_customer') }} as dim_customer ON order_head.CustomerID = dim_customer.source_customer_id 
    AND date(order_head.orderdate) BETWEEN date(dim_customer.active_from) AND IFNULL (date(dim_customer.active_to),'9999-12-31')
    --WHERE date(order_head.OrderDate) BETWEEN '2000-01-01' AND '2022-07-31'
    WHERE date(order_head.OrderDate) BETWEEN '{{ var("FROM_BIZ_DT") }}' and '{{ var("TO_BIZ_DT") }}'
)
-- CTE 
select * from fact_sales