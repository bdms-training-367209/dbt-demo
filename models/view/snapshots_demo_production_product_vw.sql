with snapshots_demo_production_product_vw as (
    SELECT 
        * EXCEPT (dbt_valid_from, dbt_valid_to)
        ,(CASE WHEN dbt_valid_from = MIN (dbt_valid_from) OVER(PARTITION BY ProductID) THEN CAST('1900-01-01' as timestamp) ELSE dbt_valid_from END) dbt_valid_from
        ,(CASE WHEN dbt_valid_to is not null THEN DATE_ADD(timestamp (dbt_valid_to), INTERVAL -1 DAY) ELSE dbt_valid_to END) dbt_valid_to
    from {{ ref("snapshots_demo_production_product") }}
)

select * from snapshots_demo_production_product_vw