{% snapshot snapshots_demo_production_product %}

{{
    config(
      target_database='bdms-training-367209',
      target_schema='demo_etl',
      unique_key='ProductID',
      strategy='check',
      check_cols=['Name','ProductNumber']
    )
}}

select * from {{ source('demo_db', 'demo_production_product') }}

{% endsnapshot %}