{% snapshot snapshots_demo_production_category %}

{{
    config(
      target_database='bdms-training-367209',
      target_schema='demo_etl',
      unique_key='product_category_id',
      strategy='check',
      check_cols=['product_category_name','product_sub_category_name']
    )
}}

select 
    {{ dbt_utils.surrogate_key('cate.ProductCategoryID', 'subcate.ProductSubCategoryID') }} as product_category_id,
    cate.Name product_category_name,
    subcate.Name product_sub_category_name,   
    cate.ProductCategoryID as source_product_category_id,
    subcate.ProductSubCategoryID source_product_sub_category_id 
from {{ source('demo_db', 'demo_production_productcategory') }} cate
INNER JOIN {{ source('demo_db', 'demo_production_productsubcategory') }} subcate ON cate.ProductCategoryID = subcate.ProductCategoryID

{% endsnapshot %}