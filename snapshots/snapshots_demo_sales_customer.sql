{% snapshot snapshots_demo_sales_customer %}

{{
    config(
      target_database='bdms-training-367209',
      target_schema='demo_etl',
      unique_key='CustomerID',
      strategy='check',
      check_cols=['AccountNumber'],
    )
}}

select * from {{ source('demo_db', 'demo_sales_customer') }}

{% endsnapshot %}
  